import TCS_CO2

# settings below for 56W # KR 
TCS_CO2.intermediate_power = 0.0
TCS_CO2.intermediate_angle = 37.8

TCS_CO2.final_power = 0.0 #1.437
TCS_CO2.final_angle = 37.8 # 52.1


#TCS_CO2.intermediate_power = 1.437
#TCS_CO2.intermediate_angle = 51.3

# settings below for 64W 
#TCS_CO2.final_power = 1.437  #1.56  
#TCS_CO2.final_angle = 51.3  #52.0 

# settings below for 72W 
#TCS_CO2.final_power = 2.40 # 2.04   
#TCS_CO2.final_angle = 56.2 # 54.5 

#TCS_CO2.final_power = 1.0 
#TCS_CO2.final_angle = 48.15
#TCS_CO2.final_power = 2.1
#TCS_CO2.final_angle = 54.15

from TCS_CO2 import *

prefix = 'TCS-ITMX_CO2'
#request = 'LASER_UP'
request = 'DOWN'
#nominal = 'LASER_UP'
#nominal = 'SET_FINAL_POWER'
nominal = 'DOWN_BASIC'
